document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('task-form');
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    form.addEventListener('submit', addTask);
    taskList.addEventListener('click', removeTask);
    document.addEventListener('DOMContentLoaded', getTasks);

    function addTask(e) {
        if(taskInput.value === '') {
            alert('Add a task');
        } else {
            const li = document.createElement('li');
            li.className = 'mt-2 bg-white p-2 rounded shadow';
            li.appendChild(document.createTextNode(taskInput.value));

            const link = document.createElement('a');
            link.className = 'delete-item secondary-content';
            link.innerHTML = '<i class="fas fa-trash"></i>';
            li.appendChild(link);

            taskList.appendChild(li);

            storeTaskInLocalStorage(taskInput.value);

            taskInput.value = '';
        }
        e.preventDefault();
    }

    function removeTask(e) {
        if(e.target.parentElement.classList.contains('delete-item')) {
            if(confirm('Are You Sure?')) {
                e.target.parentElement.parentElement.remove();

                removeTaskFromLocalStorage(e.target.parentElement.parentElement);
            }
        }
    }

    function storeTaskInLocalStorage(task) {
        let tasks;
        if(localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.push(task);

        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    function getTasks() {
        let tasks;
        if(localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.forEach(function(task) {
            const li = document.createElement('li');
            li.className = 'mt-2 bg-white p-2 rounded shadow';
            li.appendChild(document.createTextNode(task));

            const link = document.createElement('a');
            link.className = 'delete-item secondary-content';
            link.innerHTML = '<i class="fas fa-trash"></i>';
            li.appendChild(link);

            taskList.appendChild(li);
        });
    }

    function removeTaskFromLocalStorage(taskItem) {
        let tasks;
        if(localStorage.getItem('tasks') === null) {
            tasks = [];
        } else {
            tasks = JSON.parse(localStorage.getItem('tasks'));
        }

        tasks.forEach(function(task, index) {
            if(taskItem.textContent === task) {
                tasks.splice(index, 1);
            }
        });

        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
});
